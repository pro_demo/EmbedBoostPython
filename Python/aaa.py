import os
import psutil


pid = os.getpid()
p = psutil.Process(pid)
print('Process name : %s' % p.name())