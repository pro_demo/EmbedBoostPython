# -*- coding: utf-8 -*-

import asyncio
import os
import sys
import logging
logging.basicConfig(filename="ccxt.log", filemode="w", level=logging.DEBUG, format='%(asctime)s  %(levelname)s   %(funcName)s:%(lineno)d    %(message)s')
import extEngineMarket as engineMarket
# console = logging.StreamHandler()
# console.setLevel(logging.DEBUG)
# formatter = logging.Formatter("%(name)-12s: %(levelname)-8s %(message)s")
# console.setFormatter(formatter)
# logging.getLogger("").addHandler(console)

root = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(root + '/python')

import ccxt.async_support as ccxt  # noqa: E402
import threading

def task_thread(loop):
    asyncio.set_event_loop(loop)
    loop.run_forever()

event_loop = asyncio.get_event_loop()
thread = threading.Thread(target=event_loop.run_forever)
thread.start()

async def test(exchange):
    aaa = await exchange.fetch_balance()
    print(aaa)

all_exchanges = engineMarket.getExchanges()
err_len = 200

async def create_order(uuid, xid, symbol, _type, side, amount, price=None, params={}):
    try:
        logging.info('%s    %s  %s  %s  %f  %f', xid, symbol, _type, side, amount, price)
        exchange = all_exchanges[xid]
        order = await exchange.create_order(symbol, _type, side, amount, price, params)
        engineMarket.cbCreateOrder(uuid, xid, order, True)
    except Exception as e:
        logging.error('******%s', exchange.id)
        err = type(e).__name__
        engineMarket.cbCreateOrder(uuid, xid, {'info' : err}, False)
        logging.error('%s    %s  %s', exchange.id, err, str(e) if len(str(e)) < err_len else str(e)[:err_len])

def createOrder(uuid, xid, symbol, type, side, amount, price=None, params={}):
    asyncio.run_coroutine_threadsafe(create_order(uuid, xid, symbol, type, side, amount, price, params), loop=event_loop)

async def cancel_order(uuid, xid, oid, symbol=None, params={}):
    try:
        exchange = all_exchanges[xid]
        order = await exchange.cancel_order(oid, symbol, params)
        engineMarket.cbCancelOrder(uuid, xid, order)
    except Exception as e:
        err = type(e).__name__
        engineMarket.cbCancelOrder(uuid, xid, err, False)
        logging.error('%s    %s  %s', exchange.id, err, str(e) if len(str(e)) < err_len else str(e)[:err_len])

def cancelOrder(uuid, xid, oid, symbol=None, params={}):
    asyncio.run_coroutine_threadsafe(cancel_order(uuid, xid, oid, symbol, params), loop=event_loop)

async def fetch_order(uuid, xid, oid, symbol=None, params={}):
    try:
        exchange = all_exchanges[xid]
        if exchange.has['fetchOrder']:
            order = await exchange.fetch_order(oid, symbol, params={})
            engineMarket.cbFetchOrder(uuid, xid, order)
        else:
            logging.info('%s    not exist', exchange.id)
    except Exception as e:
        err = type(e).__name__
        engineMarket.cbFetchOrder(uuid, xid, err, False)
        logging.error('%s    %s  %s', exchange.id, err, str(e) if len(str(e)) < err_len else str(e)[:err_len])

def fetchOrder(uuid, xid, oid, symbol=None, params={}):
    asyncio.run_coroutine_threadsafe(fetch_order(uuid, xid, oid, symbol, params), loop=event_loop)


async def fetch_orders(uuid, xid, symbol=None, since=None, limit=None, params={}):
    try:
        exchange = all_exchanges[xid]
        if exchange.has['fetchOrders']:
            orders = await exchange.fetch_orders(symbol, since, limit, params)
            engineMarket.cbFetchOrders(uuid, xid, orders)
        else:
            logging.info('%s    not exist', exchange.id)
    except Exception as e:
        err = type(e).__name__
        engineMarket.cbFetchOrders(uuid, xid, err, False)
        logging.error('%s    %s  %s', exchange.id, err, str(e) if len(str(e)) < err_len else str(e)[:err_len])

def fetchOrders(uuid, xid, symbol=None, since=None, limit=None, params={}):
    asyncio.run_coroutine_threadsafe(fetch_orders(uuid, xid, symbol, since, limit, params), loop=event_loop)

async def fetch_open_orders(uuid, xid, symbol=None, since=None, limit=None, params={}):
    try:
        exchange = all_exchanges[xid]
        if exchange.has['fetchOpenOrders']:
            orders = await exchange.fetch_open_orders(symbol, since, limit, params)
            engineMarket.cbFetchOpenOrder(uuid, xid, orders)
        else:
            logging.info('%s    not exist', exchange.id)
    except Exception as e:
        err = type(e).__name__
        engineMarket.cbFetchOpenOrder(uuid, xid, err, False)
        logging.error('%s    %s  %s', exchange.id, err, str(e) if len(str(e)) < err_len else str(e)[:err_len])

def fetchOpenOrders(uuid, xid, symbol=None, since=None, limit=None, params={}):
    asyncio.run_coroutine_threadsafe(fetch_open_orders(uuid, xid, symbol, since, limit, params), loop=event_loop)

async def fetch_closed_orders(uuid, xid, symbol=None, since=None, limit=None, params={}):
    try:
        exchange = all_exchanges[xid]
        if exchange.has['fetchClosedOrders']:
            orders = await exchange.fetch_closed_orders(symbol, since, limit, params)
            engineMarket.cbFetchClosedOrder(uuid, xid, orders)
        else:
            logging.info('%s    not exist', exchange.id)
    except Exception as e:
        err = type(e).__name__
        engineMarket.cbFetchClosedOrder(uuid, xid, err, False)
        logging.error('%s    %s  %s', exchange.id, err, str(e) if len(str(e)) < err_len else str(e)[:err_len])

def fetchClosedOrders(uuid, xid, symbol=None, since=None, limit=None, params={}):
    asyncio.run_coroutine_threadsafe(fetch_closed_orders(uuid, xid, symbol, since, limit, params), loop=event_loop)

async def fetch_my_trades(uuid, xid, symbol=None, since=None, limit=None, params={}):
    try:
        exchange = all_exchanges[xid]
        if exchange.has['fetchMyTrades']:
            trades = await exchange.fetch_my_trades(symbol, since, limit, params)
            engineMarket.cbFetchMyTrades(uuid, xid, trades)
        else:
            logging.info('%s    not exist', exchange.id)
    except Exception as e:
        err = type(e).__name__
        engineMarket.cbFetchMyTrades(uuid, xid, err, False)
        logging.error('%s    %s  %s', exchange.id, err, str(e) if len(str(e)) < err_len else str(e)[:err_len])

def fetchMyTrades(uuid, xid, symbol=None, since=None, limit=None, params={}):
    asyncio.run_coroutine_threadsafe(fetch_my_trades(uuid, xid, symbol, since, limit, params), loop=event_loop)

async def fetch_trades(uuid, xid, symbol, since=None, limit=None, params={}):
    try:
        exchange = all_exchanges[xid]
        if exchange.has['fetchTrades']:
            trades = await exchange.fetch_trades(symbol, since, limit, params)
            engineMarket.cbFetchTrades(uuid, xid, trades)
        else:
            logging.info('%s    not exist', exchange.id)
    except Exception as e:
        err = type(e).__name__
        engineMarket.cbFetchTrades(uuid, xid, err, False)
        logging.error('%s    %s  %s', exchange.id, err, str(e) if len(str(e)) < err_len else str(e)[:err_len])

def fetchTrades(uuid, xid, symbol=None, since=None, limit=None, params={}):
    asyncio.run_coroutine_threadsafe(fetch_trades(uuid, xid, symbol, since, limit, params), loop=event_loop)


createOrder('999', 'zb', 'DASH/BTC', 'limit', 'sell', 2, 0.3)
# cancelOrder('888', 'zb', 2018122062559120, 'DASH/BTC')
# fetchOrder('111', 'zb', 2018122062559120, 'DASH/BTC')
# fetchOrders('222', 'zb', 'DASH/BTC')
# fetchOpenOrders('333', 'zb', 'DASH/BTC')
# fetchClosedOrders('444', 'zb', 'DASH/BTC')

# 2018122062551155  2018122062559120    2018122062559268    2018122062559335

# since = all_exchanges['zb'].parse8601('2018-01-01T00:00:00Z')
# def fetchOrder(uuid, xid, oid, symbol=None, params={}):
#     asyncio.get_event_loop().run_until_complete(fetch_order(uuid, xid, oid, symbol))