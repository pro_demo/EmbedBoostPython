import asyncio
import aiohttp

import time
from threading import Thread
now = lambda: time.time()

def start_loop(loop):
    asyncio.set_event_loop(loop)
    loop.run_forever()
def more_work(x):
    print('More work {}'.format(x))
    time.sleep(x)
    print('Finished more work {}'.format(x))

start = now()
new_loop = asyncio.new_event_loop()
t = Thread(target=start_loop, args=(new_loop,))
t.start()
print('TIME: {}'.format(time.time() - start))

new_loop.call_soon_threadsafe(more_work, 6)
new_loop.call_soon_threadsafe(more_work, 3)



async def fetch(session, url):
    async with session.get(url) as response:
        return await response.text()

api_key = 'vbFfzCCFoRuoGVU8vumgXl8LElVg8Sr2'
base_uri = 'http://forex.1forge.com/1.0.3/'

async def getForexSymbols():
    async with aiohttp.ClientSession() as session:
        async with session.get(base_uri + 'symbols?cache=false' + '&api_key=vbFfzCCFoRuoGVU8vumgXl8LElVg8Sr2') as response:
            json = await response.json()
            print(json)

async def getForexQuotes(pairs):
    async with aiohttp.ClientSession() as session:
        async with session.get(base_uri + 'quotes?pairs=' + ','.join(pairs) + '&api_key=vbFfzCCFoRuoGVU8vumgXl8LElVg8Sr2') as response:
            json = await response.json()
            print(json)

async def MarketIsOpen():
    async with aiohttp.ClientSession() as session:
        async with session.get(base_uri + 'market_status?cache=false' + '&api_key=vbFfzCCFoRuoGVU8vumgXl8LElVg8Sr2') as response:
            json = await response.json()
            print(json)

async def getForexQuota():
    async with aiohttp.ClientSession() as session:
        async with session.get(base_uri + 'quota?cache=false' + '&api_key=vbFfzCCFoRuoGVU8vumgXl8LElVg8Sr2') as response:
            json = await response.json()
            print(json)


if __name__ == '__main__':
loop = asyncio.get_event_loop()
loop.run_until_complete(getForexSymbols())
loop.run_until_complete(getForexQuotes([]))
loop.run_until_complete(MarketIsOpen())