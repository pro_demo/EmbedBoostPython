#include <iostream>
#include <boost/python.hpp>
#include <Python.h>
// python::class_
// python::init
// python::def
using namespace boost::python;
std::string parse_python_exception();

struct World
{
	World(std::string msg) : msg(msg) {} // added constructor
	World(double d) : age(d) {}
	void set(std::string msg) { this->msg = msg; }
	std::string greet() { return msg; }
	std::string msg;
	double age;
};
BOOST_PYTHON_MODULE(hello)
{
	class_<World>("World", init<std::string>())
		.def("greet", &World::greet)
		.def(init<double>())
		.def("set", &World::set);
}

struct Var
{
	Var(std::string name) : name(name), value() {}
	std::string const name;
	float value;
};
BOOST_PYTHON_MODULE(var)
{
	class_<Var>("Var", init<std::string>())
		.def_readonly("name", &Var::name)
		.def_readwrite("value", &Var::value);
}

struct Num
{
	Num() {}
	float get() const {return _value;}
	void set(float value) {_value = value;}
	float _value;
};
BOOST_PYTHON_MODULE(num)
{
	class_<Num>("Num")
		.add_property("rovalue", &Num::get)
		.add_property("value", &Num::get, &Num::set);
}

struct Base
{
	virtual ~Base() {}
//	virtual int f() = 0;
	virtual int f() { std::cout << "Call Base f()" << std::endl; return 1; }
};
struct Derived : Base
{
	virtual int f() { std::cout << "Call Derived f()" << std::endl; return 2; }
};
struct BaseWrap : Base, wrapper<Base>
{
	int f()
	{
		//return this->get_override("f")();
		if (override f = this->get_override("f"))
			return f(); // *note*
		return Base::f();
	}
	int default_f() { return this->Base::f(); }
};
void b(Base*) { std::cout << "Call b(Base*)" << std::endl; }
void d(Derived*) { std::cout << "Call d(Derived*)" << std::endl; }
Base* factory() { std::cout << "Call factory()" << std::endl; return new Derived; }
BOOST_PYTHON_MODULE(poly)
{
	class_<BaseWrap, boost::noncopyable>("Base")
		.def("f", &Base::f, &BaseWrap::default_f)
		;
	class_<Derived, bases<Base> >("Derived")
		;
	def("b", b);
	def("d", d);
	def("factory", factory, return_value_policy<manage_new_object>());// Tell Python to take ownership of factory's result	
}

int main()
{
	if (PyImport_AppendInittab("hello", PyInit_hello) == -1)return 0;
	if (PyImport_AppendInittab("var", PyInit_var) == -1)return 0;
	if (PyImport_AppendInittab("num", PyInit_num) == -1)return 0;
	if (PyImport_AppendInittab("poly", PyInit_poly) == -1)return 0;
	Py_Initialize();

	namespace python = boost::python;
	python::object main = python::import("__main__");
	python::object global(main.attr("__dict__"));
	try
	{
		python::exec(
			"import hello			\n"
			"planet = hello.World('sun')	\n"
			"planet.greet()			\n"
			"planet.set('moon')		\n"
			"planet.greet()			\n"
			, global, global);

		python::exec(
			"import var			\n"
			"x = var.Var('pi')	\n"
			"x.value = 3.14		\n"
			"print(x.name, 'is around', x.value)	\n"
			//"x.name = 'e'		\n"	// # can't change name
			, global, global);

		python::exec(
			"import num			\n"
			"x = num.Num()		\n"
			"x.value = 3.14		\n"
			"print(x.value, x.rovalue)	\n"
			//"x.rovalue = 2.17	\n"	//  # error!	
			, global, global);
	}
	catch (boost::python::error_already_set const &)
	{
		std::string perror_str = parse_python_exception();
		std::cout << "Error in Python: " << perror_str << std::endl;
	}
	try
	{
		python::exec_file("poly.py", global, global);
	}
	catch (boost::python::error_already_set const &)
	{
		std::string perror_str = parse_python_exception();
		std::cout << "Error in Python: " << perror_str << std::endl;
	}
	try
	{
		python::object mod_hello = python::import("hello");
		python::object att_World = mod_hello.attr("World");
		python::object cls_World = att_World("Class_World_1");
		python::object func_set = cls_World.attr("set");
		python::object func_greet = cls_World.attr("greet");
		std::cout << python::extract<char*>(func_greet()) << std::endl;
		func_set("Class_World_2");
		std::cout << python::extract<char*>(func_greet()) << std::endl;

		World& world = extract<World&>(cls_World);
		world.set("Class_World_3");
		std::cout << world.greet() << std::endl;
		std::cout << python::extract<char*>(func_greet()) << std::endl;


		python::object mod_var = python::import("var");
		python::object att_Var = mod_var.attr("Var");
		python::object cls_Var = att_Var("var_name");

		python::object obj_name = cls_Var.attr("name");
		auto name = python::extract<char*>(obj_name);
		std::cout << name << std::endl;
		//python::object obj_value = cls_Var.attr("value");
		//auto value = python::extract<double*>(obj_value);
		//std::cout << value << std::endl;
	}
	catch (boost::python::error_already_set const &)
	{
		std::string perror_str = parse_python_exception();
		std::cout << "Error in Python: " << perror_str << std::endl;
	}
	return 1;
}

std::string parse_python_exception() {
	namespace py = boost::python;

	PyObject *type_ptr = NULL, *value_ptr = NULL, *traceback_ptr = NULL;
	// Fetch the exception info from the Python C API
	PyErr_Fetch(&type_ptr, &value_ptr, &traceback_ptr);

	// Fallback error
	std::string ret("Unfetchable Python error");
	// If the fetch got a type pointer, parse the type into the exception string
	if (type_ptr != NULL) {
		py::handle<> h_type(type_ptr);
		py::str type_pstr(h_type);
		// Extract the string from the boost::python object
		py::extract<std::string> e_type_pstr(type_pstr);
		// If a valid string extraction is available, use it 
		//  otherwise use fallback
		if (e_type_pstr.check())
			ret = e_type_pstr();
		else
			ret = "Unknown exception type";
	}
	// Do the same for the exception value (the stringification of the exception)
	if (value_ptr != NULL) {
		py::handle<> h_val(value_ptr);
		py::str a(h_val);
		py::extract<std::string> returned(a);
		if (returned.check())
			ret += ": " + returned();
		else
			ret += std::string(": Unparseable Python error: ");
	}
	// Parse lines from the traceback using the Python traceback module
	if (traceback_ptr != NULL) {
		py::handle<> h_tb(traceback_ptr);
		// Load the traceback module and the format_tb function
		py::object tb(py::import("traceback"));
		py::object fmt_tb(tb.attr("format_tb"));
		// Call format_tb to get a list of traceback strings
		py::object tb_list(fmt_tb(h_tb));
		// Join the traceback strings into a single string
		py::object tb_str(py::str("\n").join(tb_list));
		// Extract the string, check the extraction, and fallback in necessary
		py::extract<std::string> returned(tb_str);
		if (returned.check())
			ret += ": " + returned();
		else
			ret += std::string(": Unparseable Python traceback");
	}
	return ret;
}
