#! /usr/bin/env python
# Copyright Stefan Seefeld 2006. Distributed under the Boost
# Software License, Version 1.0. (See accompanying
# file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
from poly import *

class PyDerived(Base):
    def f(self):
        print('Call PyDerived f()')
        return 42
base = Base()
derived = Derived()
pyderived = PyDerived()
base.f()
derived.f()
pyderived.f()
b(base)
d(derived)
#d(pyderived)	error
bb = factory()
bb.f()
