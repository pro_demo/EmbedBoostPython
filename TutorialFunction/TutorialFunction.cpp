#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/args.hpp>
#include <boost/python/tuple.hpp>
#include <boost/python/class.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/return_internal_reference.hpp>

#include <iostream>
#include <boost/python.hpp>
#include <Python.h>
// file:///C:/Program%20Files%20(x86)/boost_1_67_0/libs/python/doc/html/reference/function_invocation_and_creation/boost_python_overloads_hpp.html#function_invocation_and_creation.boost_python_overloads_hpp.macros
using namespace boost::python;
std::string parse_python_exception();

tuple f(int x = 1, double y = 4.25, char const* z = "wow")
{
	return make_tuple(x, y, z);
}

BOOST_PYTHON_FUNCTION_OVERLOADS(f_overloads, f, 0, 3)

struct X
{
	tuple f(int x, double y = 4.25, char const* z = "wow")
	{
		return make_tuple(x, y, z);
	}
};

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(f_member_overloads, f, 1, 3)

BOOST_PYTHON_MODULE(args_ext)
{
	def("f", f,
		f_overloads());
//		f_overloads(args("x", "y", "z"), "This is f's docstring"));// 也可以

	class_<X>("X", "This is X's docstring")
		.def("f1", &X::f,
			f_member_overloads());
//			f_member_overloads(args("x", "y", "z"), "f's docstring"));// 也可以
}

int main()
{
	if (PyImport_AppendInittab("args_ext", PyInit_args_ext) == -1)return 0;
	Py_Initialize();

	namespace python = boost::python;
	python::object main = python::import("__main__");
	python::object global(main.attr("__dict__"));
	try
	{
		python::exec(
			"import args_ext		\n"
			"print(args_ext.f())	\n"
			"print(args_ext.f(2))	\n"
			"print(args_ext.f(3, 3.3))			\n"
			"print(args_ext.f(4, 4.4, '444'))	\n"
			"ax = args_ext.X()			\n"
			"print(ax.f1(2))			\n"
			"print(ax.f1(3, 3.3))		\n"
			"print(ax.f1(4, 4.4, '444'))\n"
			, global, global);

	}
	catch (boost::python::error_already_set const &)
	{
		std::string perror_str = parse_python_exception();
		std::cout << "Error in Python: " << perror_str << std::endl;
	}
	return 1;
}

std::string parse_python_exception() {
	namespace py = boost::python;

	PyObject *type_ptr = NULL, *value_ptr = NULL, *traceback_ptr = NULL;
	// Fetch the exception info from the Python C API
	PyErr_Fetch(&type_ptr, &value_ptr, &traceback_ptr);

	// Fallback error
	std::string ret("Unfetchable Python error");
	// If the fetch got a type pointer, parse the type into the exception string
	if (type_ptr != NULL) {
		py::handle<> h_type(type_ptr);
		py::str type_pstr(h_type);
		// Extract the string from the boost::python object
		py::extract<std::string> e_type_pstr(type_pstr);
		// If a valid string extraction is available, use it 
		//  otherwise use fallback
		if (e_type_pstr.check())
			ret = e_type_pstr();
		else
			ret = "Unknown exception type";
	}
	// Do the same for the exception value (the stringification of the exception)
	if (value_ptr != NULL) {
		py::handle<> h_val(value_ptr);
		py::str a(h_val);
		py::extract<std::string> returned(a);
		if (returned.check())
			ret += ": " + returned();
		else
			ret += std::string(": Unparseable Python error: ");
	}
	// Parse lines from the traceback using the Python traceback module
	if (traceback_ptr != NULL) {
		py::handle<> h_tb(traceback_ptr);
		// Load the traceback module and the format_tb function
		py::object tb(py::import("traceback"));
		py::object fmt_tb(tb.attr("format_tb"));
		// Call format_tb to get a list of traceback strings
		py::object tb_list(fmt_tb(h_tb));
		// Join the traceback strings into a single string
		py::object tb_str(py::str("\n").join(tb_list));
		// Extract the string, check the extraction, and fallback in necessary
		py::extract<std::string> returned(tb_str);
		if (returned.check())
			ret += ": " + returned();
		else
			ret += std::string(": Unparseable Python traceback");
	}
	return ret;
}
